<?
$h1         = 'Clientes';
$title      = 'Clientes';
$desc       = 'Clientes: A empresa foi fundada em 1989 e durante esses anos tem mantido uma posição de destaque no mercado. Nossa principal especialização é manutenção';
$key        = 'uuuuuuuuuu, jjjjjjjjjjjj, lllllllllll';
$var        = 'Clientes';
include('inc/head.php');
?>
<script src="<?=$url?>js/owl.carousel.js" ></script>
<link rel="stylesheet" href="<?=$url?>css/owl.carousel.css">
<link rel="stylesheet" href="<?=$url?>css/owl.theme.css">
<script>
$(document).ready(function() {
$(".owl-demo").owlCarousel({
autoPlay: 3000,
items : 5, // Quantidade de itens que irá aparecer
itemsDesktopSmall : [979,4],
itemsTablet : [768, 3],
itemsMobile : [600, 1]
});
});
</script>
</head>
<body>
<? include('inc/topo.php');?>
<main>
    <div class="content">
        <section>
            <div class="wrapper">
                <?=$caminho?>
                <h1><?=$h1?></h1>
                <hr class="line">
            </div>
            <article class="full">
                
                
                <div class="conteudo-index">
                    <div class="wrapper">
                        <div class="owl-carousel owl-theme centralizar owl-demo">
                            
                            <?php
                            for ($i = 1; $i <= 8; $i++) {
                            $i < 10 ? $zero = 0 : $zero = "";
                            echo "
                            <div class=\"item\"><a href=\"".$url."imagens/clientes/cliente-$zero".$i.".jpg\" data-fancybox=\"group1\" title=\"".$h1."\" class=\"lightbox\" ><img src=\"".$url."imagens/clientes/cliente-$zero".$i.".jpg\" alt=\"".$h1."\" title=\"".$h1."\"  /></a></div>
                            ";
                            }
                            ?>
                        </div>
                    </div>
                </div>
                <div class="wrapper">
                    <h2 class="h2-titulo">Produtos em Destaque</h2>
                    <hr class="line">
                    <ul class="thumbnails">
                        <li>
                            <a rel="nofollow" href="<?=$url;?>empresa-manutencao-geradores" title="Empresa de manutenção de geradores"><img src="<?=$url;?>imagens/informacoes/empresa-manutencao-geradores-01.jpg" alt="Empresa de manutenção de geradores" title="Empresa de manutenção de geradores"/></a>
                            <h2><a href="<?=$url;?>empresa-manutencao-geradores" title="Empresa de manutenção de geradores">Empresa de manutenção de geradores</a></h2>
                        </li>
                        <li>
                            <a rel="nofollow" href="<?=$url;?>empresas-geradores-sp" title="Empresas de geradores em SP"><img src="<?=$url;?>imagens/informacoes/empresas-geradores-sp-01.jpg" alt="Empresas de geradores em SP" title="Empresas de geradores em SP"/></a>
                            <h2><a href="<?=$url;?>empresas-geradores-sp" title="Empresas de geradores em SP">Empresas de geradores em SP</a></h2>
                        </li>
                        <li>
                            <a rel="nofollow" href="<?=$url;?>instalacao-geradores-diesel" title="Instalação de geradores diesel"><img src="<?=$url;?>imagens/informacoes/instalacao-geradores-diesel-01.jpg" alt="Instalação de geradores diesel" title="Instalação de geradores diesel"/></a>
                            <h2><a href="<?=$url;?>instalacao-geradores-diesel" title="Instalação de geradores diesel">Instalação de geradores diesel</a></h2>
                        </li>
                        <li>
                            <a rel="nofollow" href="<?=$url;?>isolamento-acustico-geradores" title="Isolamento acústico de geradores"><img src="<?=$url;?>imagens/informacoes/isolamento-acustico-geradores-01.jpg" alt="Isolamento acústico de geradores" title="Isolamento acústico de geradores"/></a>
                            <h2><a href="<?=$url;?>isolamento-acustico-geradores" title="Isolamento acústico de geradores">Isolamento acústico de geradores</a></h2>
                        </li>
                    </ul>
                </div>
                
                
                <div class="clear"></div>
                
            </article>
        </section>
    </div>
</main>
<? include('inc/footer.php');?>
</body>
</html>