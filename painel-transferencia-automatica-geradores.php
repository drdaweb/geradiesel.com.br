<?php
include('inc/vetKey.php');
$h1             = "Painel de transferência automática para geradores";
$title          = $h1;
$desc           = "Fazer o manuseio do painel de transferência automática para geradores requer muita cautela, portanto é importante que somente pessoas altamente preparadas";
$key            = "painel,transferencia,automatica,geradores";
$legendaImagem  = "Foto ilustrativa de Painel de transferência automática para geradores";
$pagInterna     = "Informações";
$urlPagInterna  = "informacoes";
include('inc/head.php');
include('inc/fancy.php');
?>
<script defer src="<?=$url?>js/organictabs.jquery.js" ></script>
<?php include("inc/type-search.php")?>
</head>
<body>

<? include('inc/topo.php');?>
<div class="wrapper">
  <main>
    <div class="content">
      <section>
        <?=$caminho2?>
        <h1><?=$h1?></h1>
        <article>
          <? $quantia = 3; include('inc/gallery.php');?>
          <p class="alerta">Clique nas imagens para ampliar</p>
          <h2>VEJA PARA QUE SERVE UM PAINEL DE TRANSFERÊNCIA AUTOMÁTICA PARA GERADORES</h2>
          <p>Todos nós sabemos das vantagens de adquirir um gerador de energia, pois o robusto equipamento é capaz de fornecer energia elétrica por diversas horas seguidas em casos de queda de energia. Mas existe um componente que é extremamente importante no grupo de geradores e ele é o <strong>painel de transferência automática para geradores</strong>. Esse equipamento também pode ser conhecido como QTA, que é o quadro de transferência padrão que consiste na utilização de contatores ou chave reversora motorizada para transferência de alimentação de carga, essa são responsáveis por converter a energia  fornecida pela concessionaria de energia e pelo grupo gerador, o sistema é intertravado mecanicamente e eletricamente, impossibilitando que ambas alimentações entrem em curto ao alimentar as cargas de emergência.</p>
          <p>Fazer o manuseio do <strong>painel de transferência automática para geradores</strong> requer muita cautela, portanto é importante que somente pessoas altamente preparadas realizem o manuseio do painel.</p>
          <p>Atualmente podemos encontrar muitas empresas que são responsáveis em fornecer o <strong>painel de transferência automática para geradores</strong>, mas antes de fazer a contratação, é necessário verificar e analisar se a mesma possui bons profissionais e equipamentos de qualidade para que o fornecimento do serviço possa ser realizado com eficiência e segurança.</p>
          <h3>GERADIESEL É UMA EMPRESA DE PAINEL DE TRANSFERÊNCIA AUTOMÁTICA PARA GERADORES</h3>
          <p>Como nós da empresa Geradiesel somos atentos às necessidades do mercado e, dessa forma, somos fornecedores de <strong>painel de transferência automática para geradores</strong>, nossa sede está localizada na cidade de São Paulo.</p>
          <p>Atualmente, somos menção quando o assunto é <strong>painel de transferência automática para geradores</strong>, mas para isso, é necessário praticar diariamente os valores e a nossa missão, já que colocamos o cliente em primeiro lugar. Nossa missão é manter a transparência e respeito com nossos clientes e parceiros e nossos valores são fornecer produtos de qualidade inquestionável.</p>
          <p>Para saber mais sobre <strong>painel de transferência automática para geradores</strong>, ou qualquer outro produto e serviço que fazemos, basta entrar em contato com o nosso setor comercial e solicitar um orçamento com um de nossos vendedores. Estamos prontos para te atender da melhor forma.</p>
          <? include('inc/saiba-mais.php');?>
          <? include('inc/social-media.php');?>
        </article>
        <? include('inc/coluna-lateral.php');?>
        <br class="clear" />
        <? include('inc/paginas-relacionadas.php');?>
        <? include('inc/regioes.php');?>
        <br class="clear">
        <? include('inc/copyright.php');?>
      </section>
    </div>
  </main>
  </div><!-- .wrapper -->
  <? include('inc/footer.php');?>
</body>
</html>