<?
$h1         = 'Empresa';
$title      = 'Empresa';
$desc       = 'A empresa foi fundada em 1989 e durante esses anos tem mantido uma posição de destaque no mercado. Nossa principal especialização é manutenção preventiva';
$key        = 'uuuuuuuuuu, jjjjjjjjjjjj, lllllllllll';
$var        = 'Empresa';
include('inc/head.php');
?>
<script src="<?=$url?>js/owl.carousel.js" ></script>
<link rel="stylesheet" href="<?=$url?>css/owl.carousel.css">
<link rel="stylesheet" href="<?=$url?>css/owl.theme.css">
<script>
$(document).ready(function() {
$(".owl-demo").owlCarousel({
autoPlay: 3000,
items : 5, // Quantidade de itens que irá aparecer
itemsDesktopSmall : [979,4],
itemsTablet : [768, 3],
itemsMobile : [600, 1]
});
});
</script>
</head>
<body>
<? include('inc/topo.php');?>
<main>
    <div class="content">
        <section>
            <div class="wrapper">
                <?=$caminho?>
                <h1><?=$h1?></h1>
                <hr class="line">
            </div>
            <article class="full">
                
                <div class="wrapper">
                    <div class="grid flex ">
                        <div class="col-6">
                            <p>A empresa foi fundada em 1989 e durante esses anos tem mantido uma posição de destaque no mercado. Nossa principal especialização é manutenção preventiva e corretiva em grupos geradores.</p>
                            <p>Nossa empresa dispõe de profissionais altamente qualificados que estão constantemente melhorando seus métodos e conhecimentos. Atendemos às necessidades de nossos clientes com máxima prioridade. Isto é o que nos coloca como ponto de referência no mercado.</p>
                            <h2>Visão</h2>
                            <p>Garantir um atendimento de excelente qualidade aos nosso clientes.</p>
                            <h2>Missão</h2>
                            <p>Ser a maior empresa de prestação de serviços em grupos geradores do estado de São Paulo e superar os padrões de excelência no atendimento ao cliente.</p>
                        </div>
                        <div class="col-6">
                            <img src="<?=$url?>imagens/geradiesel-valores.jpg"  alt="Empresa" title="Empresa" class="picture-center ">
                        </div>
                    </div>
                    <div class="clear"></div>
                    <div class="grid flex">
                        <div class="col-6">
                            <img src="<?=$url?>imagens/geradiesel.jpg"  alt="Empresa" title="Empresa" class="picture-center ">
                        </div>
                        <div class="col-6">
                            <h2>Valores</h2>
                            <h2>∕∕ Ética e Transparência </h2>
                            <p>Nossos negócios, ações, compromissos, são orientados respeitando as leis, os princípios morais e do bem estar.</p>
                            <h2>∕∕ Responsabilidade econômica, social e ambiental</h2>
                            <p>Reconhecemos e agimos para o o sucesso dos negócios com uma perspectiva de longo prazo, contribuindo para o desenvolvimento econômico, social e ambiental.</p>
                            <h2> ∕∕ Respeito à vida </h2>
                            <p>Nosso respeito pela vida não se detém apenas na pessoa, mas sim as suas formas de manifestações e situações. Quando não existe essa manifestação, os valores éticos se enfraquecem é por isso que buscamos a excelência nas questões de saúde, segurança e meio ambiente.</p>
                        </div>
                    </div>
                    
                </div>
                <div class="clear"></div>
                <? include('inc/produtos-destaques-clientes.php'); ?>
                <div class="clear"></div>
                
            </article>
        </section>
    </div>
</main>

<? include('inc/footer.php');?>
</body>
</html>