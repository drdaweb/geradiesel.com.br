<header id="scrollheader">
    <div class="topo">
        <div class="wrapper">
            <span class="hide-mobile fright"><i class="fa fa-map-marker" aria-hidden="true"></i> <?=$rua." - ".$cidade."-".$UF?></span>
            <a rel="nofollow" class="fleft" title="Clique e ligue" href="tel:<?=$ddd.$fone?>"><i class="fa fa-phone" aria-hidden="true"></i> <?=$ddd?> <strong><?=$fone?></strong></a>
            

            <a rel="nofollow" class="social-icons fleft" href="mailto:<?=$emailContato?>" target="_blank" title="Envie um E-mail"><i class="fa fa-envelope"></i> atendimento@geradiesel.com.br</a>
            <div class="clear"></div>
        </div>
    </div>
    <div class="wrapper">
        <div class="logo"><a rel="nofollow" href="<?=$url?>" title="Voltar a página inicial"><img src="<?=$url?>imagens/logo.png" alt="<?=$slogan." - ".$nomeSite?>" title="<?=$slogan." - ".$nomeSite?>"></a></div>
        <nav id="menu2">
            <ul >
                <? include('inc/menu-top.php');?>
            </ul>
        </nav>
    </div>

    <div class="clear"></div>

</header>
<div id="header-block"></div>