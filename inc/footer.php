<div class="clear"></div>
<footer>
	<div class="wrapper">
		<div class="contact-footer">
			<address>
				<span><?=$nomeSite." - ".$slogan?></span>
				<?=$rua." - ".$bairro?> <br>
				<?=$cidade."-".$UF." - ".$cep?>
			</address>
			<a href="tel:<?=$ddd.$fone?>" title="Clique e Ligue"><?=$ddd?> <strong><?=$fone?></strong></a>
			<?php
			if(isset($fone2) && !empty($fone2)) { echo " / <a href=\"tel:$ddd$fone2\" title=\"Clique e Ligue\">$ddd <strong>$fone2</strong></a>";}
			if(isset($fone3) && !empty($fone3)) { echo " / <a href=\"tel:$ddd$fone3\" title=\"Clique e Ligue\">$ddd <strong>$fone3</strong></a>";}
			?>
			<br>
		</div>
		<div class="menu-footer">
			<nav>
				<ul>
					<li><a rel="nofollow" href="<?=$url?>" title="Página inicial">Home</a></li>
					<li><a rel="nofollow" href="<?=$url?>empresa" title="Sobre a Empresa <?=$nomeSite?>">Empresa</a></li>
					<li><a rel="nofollow" href="<?=$url?>servicos" title="Serviços <?=$nomeSite?>">Serviços</a></li>
					<!-- <li><a rel="nofollow" href="<?=$url?>clientes" title="Clientes <?=$nomeSite?>">Clientes</a></li> -->
					<li><a rel="nofollow" href="<?=$url?>informacoes" title="Informações <?=$nomeSite?>">Informações</a></li>
					<li><a rel="nofollow" href="<?=$url?>contato" title="Fale com a <?=$nomeSite?>">Contato</a></li>
					<li><a href="<?=$url?>mapa-site" title="Mapa do site <?=$nomeSite?>">Mapa do site</a></li>
				</ul>
			</nav>
		</div>
		<? include('inc/canais.php');?>
		<br class="clear">
	</div>
</footer>
<div class="copyright-footer">
	<div class="wrapper">
		Copyright © <?=$nomeSite?>. (Lei 9610 de 19/02/1998)
		<div class="selos">
			<a rel="nofollow" href="https://validator.w3.org/check?uri=<?=$url.$urlPagina?>" target="_blank" title="HTML5 W3C"><i class="fa fa-html5"></i> <strong>W3C</strong></a>
			<a rel="nofollow" href="https://jigsaw.w3.org/css-validator/validator?uri=<?=$url.$urlPagina?>&profile=css3svg&usermedium=all&warning=1&vextwarning=&lang=pt-BR" target="_blank" title="CSS W3C" ><i class="fa fa-css3"></i> <strong>W3C</strong></a>
			<img src="<?=$url?>imagens/selo.png" alt="Selo" />
		</div>
	</div>
</div>
<?php include('inc/LAB.php') ?>
<script>
  $(window).load(function() {
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.defer=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
      ga('create', '<?=$idAnalytics?>', 'auto');
      ga('send', 'pageview');

      $LAB
      .script("js/geral.js").wait()
      .script("js/jquery.scrollUp.min.js").wait()
      .script("js/scroll.js").wait(function(){
          initMyPage();
      });


      jQuery(document).ready(function ($) {
          jQuery('.btn-orc').on('click', function() {
              ga('send', 'event', 'Evento Orcamento','Clique', 'Clique Orcamento');
          });
      });
    });   

      // Disparando função através do scroll
    $(window).on('scroll', function(e){
    var keyscroll;
      // criando uma condição se a posição na tela for maior que 300px e o valor da variavel  for diferente de true executa
        if($(this).scrollTop() >= 100 && !keyscroll){
      // aleterando o valor da variável para que não dispare novamente a função
            keyscroll = true;
        }
    });
</script>

<script>
    var myTime = window.performance.now();
    var items = window.performance.getEntriesByType('mark');
    var items = window.performance.getEntriesByType('measure');
    var items = window.performance.getEntriesByName('mark_fully_loaded');
    window.performance.mark('mark_fully_loaded');
    window.performance.measure('measure_load_from_dom', 'mark_fully_loaded');  
    window.performance.clearMarks();
    window.performance.clearMeasures('measure_load_from_dom');
</script>