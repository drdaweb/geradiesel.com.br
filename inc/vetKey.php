
  <?php
  $vetKey = array();
  
    $vetKey[0] = array("url" => "assistencia-tecnica-gerador", "key" => "Assistência técnica de gerador", "desc" =>"");
    
    $vetKey[1] = array("url" => "automacao-geradores-energia", "key" => "Automação de geradores de energia", "desc" =>"");
    
    $vetKey[2] = array("url" => "automacao-grupos-geradores", "key" => "Automação de grupos geradores", "desc" =>"");
    
    $vetKey[3] = array("url" => "automacao-geradores", "key" => "Automação para geradores", "desc" =>"");
    
    $vetKey[4] = array("url" => "cabine-acustica-gerador", "key" => "Cabine acústica para gerador", "desc" =>"");
    
    $vetKey[5] = array("url" => "cabine-acustica-grupo-gerador", "key" => "Cabine acústica para grupo gerador", "desc" =>"");
    
    $vetKey[6] = array("url" => "conserto-gerador-energia", "key" => "Conserto de gerador de energia", "desc" =>"");
    
    $vetKey[7] = array("url" => "conserto-grupo-gerador", "key" => "Conserto de grupo gerador", "desc" =>"");
    
    $vetKey[8] = array("url" => "contrato-manutencao-preventiva-geradores", "key" => "Contrato de manutenção preventiva de geradores", "desc" =>"");
    
    $vetKey[9] = array("url" => "empresa-geradores", "key" => "Empresa de geradores", "desc" =>"");
    
    $vetKey[10] = array("url" => "empresa-manutencao-geradores", "key" => "Empresa de manutenção de geradores", "desc" =>"");
    
    $vetKey[11] = array("url" => "empresa-manutencao-geradores-sp", "key" => "Empresa de manutenção de geradores SP", "desc" =>"");
    
    $vetKey[12] = array("url" => "empresa-manutencao-preventiva-geradores", "key" => "Empresa de manutenção preventiva de geradores", "desc" =>"");
    
    $vetKey[13] = array("url" => "empresa-especializada-manutencao-geradores", "key" => "Empresa especializada em manutenção de geradores", "desc" =>"");
    
    $vetKey[14] = array("url" => "empresas-geradores-diesel", "key" => "Empresas de geradores diesel", "desc" =>"");
    
    $vetKey[15] = array("url" => "empresas-geradores-sp", "key" => "Empresas de geradores em SP", "desc" =>"");
    
    $vetKey[16] = array("url" => "empresas-grupos-geradores", "key" => "Empresas de grupos geradores", "desc" =>"");
    
    $vetKey[17] = array("url" => "empresas-manutencao-grupo-gerador", "key" => "Empresas de manutenção de grupo gerador", "desc" =>"");
    
    $vetKey[18] = array("url" => "instalacao-gerador-energia-eletrica", "key" => "Instalação de gerador de energia elétrica", "desc" =>"");
    
    $vetKey[19] = array("url" => "instalacao-geradores-diesel", "key" => "Instalação de geradores diesel", "desc" =>"");
    
    $vetKey[20] = array("url" => "instalacao-grupos-geradores", "key" => "Instalação de grupos geradores", "desc" =>"");
    
    $vetKey[21] = array("url" => "instalacao-oxicatalisador", "key" => "Instalação de oxicatalisador", "desc" =>"");
    
    $vetKey[22] = array("url" => "instalacao-gerador-energia", "key" => "Instalação gerador de energia", "desc" =>"");
    
    $vetKey[23] = array("url" => "instalacao-grupo-gerador-diesel", "key" => "Instalação grupo gerador diesel", "desc" =>"");
    
    $vetKey[24] = array("url" => "isolamento-acustico-geradores", "key" => "Isolamento acústico de geradores", "desc" =>"");
    
    $vetKey[25] = array("url" => "isolamento-acustico-grupo-gerador", "key" => "Isolamento acústico grupo gerador", "desc" =>"");
    
    $vetKey[26] = array("url" => "isolamento-acustico-sala-geradores", "key" => "Isolamento acústico para sala de geradores", "desc" =>"");
    
    $vetKey[27] = array("url" => "manutencao-corretiva-e-preventiva-geradores-energia", "key" => "Manutenção corretiva e preventiva de geradores de energia", "desc" =>"");
    
    $vetKey[28] = array("url" => "manutencao-corretiva-geradores", "key" => "Manutenção corretiva em geradores", "desc" =>"");
    
    $vetKey[29] = array("url" => "manutencao-geradores", "key" => "Manutenção de geradores", "desc" =>"");
    
    $vetKey[30] = array("url" => "manutencao-geradores-energia", "key" => "Manutenção de geradores de energia", "desc" =>"");
    
    $vetKey[31] = array("url" => "manutencao-grupos-geradores", "key" => "Manutenção de grupos geradores", "desc" =>"");
    
    $vetKey[32] = array("url" => "manutencao-geradores-diesel", "key" => "Manutenção geradores a diesel", "desc" =>"");
    
    $vetKey[33] = array("url" => "manutencao-geradores-sp", "key" => "Manutenção geradores SP", "desc" =>"");
    
    $vetKey[34] = array("url" => "manutencao-grupo-gerador-diesel", "key" => "Manutenção grupo gerador diesel", "desc" =>"");
    
    $vetKey[35] = array("url" => "manutencao-grupo-gerador-sp", "key" => "Manutenção grupo gerador SP", "desc" =>"");
    
    $vetKey[36] = array("url" => "manutencao-preditiva-geradores", "key" => "Manutenção preditiva em geradores", "desc" =>"");
    
    $vetKey[37] = array("url" => "manutencao-preventiva-geradores-energia", "key" => "Manutenção preventiva de geradores de energia", "desc" =>"");
    
    $vetKey[38] = array("url" => "manutencao-preventiva-grupos-geradores", "key" => "Manutenção preventiva de grupos geradores", "desc" =>"");
    
    $vetKey[39] = array("url" => "manutencao-preventiva-geradores-diesel", "key" => "Manutenção preventiva geradores diesel", "desc" =>"");
    
    $vetKey[40] = array("url" => "oxicatalisador-gerador", "key" => "Oxicatalisador para gerador", "desc" =>"");
    
    $vetKey[41] = array("url" => "oxicatalisador-grupo-gerador", "key" => "Oxicatalisador para grupo gerador", "desc" =>"");
    
    $vetKey[42] = array("url" => "painel-transferencia-automatica-geradores", "key" => "Painel de transferência automática para geradores", "desc" =>"");
    
    $vetKey[43] = array("url" => "painel-transferencia-gerador", "key" => "Painel de transferência gerador", "desc" =>"");
    
    $vetKey[44] = array("url" => "pecas-reposicao-geradores", "key" => "Peças de reposição para geradores", "desc" =>"");
    
    $vetKey[45] = array("url" => "pecas-e-acessorios-geradores", "key" => "Peças e acessórios para geradores", "desc" =>"");
    
    $vetKey[46] = array("url" => "pecas-geradores", "key" => "Peças para geradores", "desc" =>"");
    
    $vetKey[47] = array("url" => "pecas-geradores-diesel", "key" => "Peças para geradores a diesel", "desc" =>"");
    
    $vetKey[48] = array("url" => "pecas-geradores-energia", "key" => "Peças para geradores de energia", "desc" =>"");
    
    $vetKey[49] = array("url" => "pecas-grupo-geradores", "key" => "Peças para grupo geradores", "desc" =>"");
    
    $vetKey[50] = array("url" => "quadro-transferencia-automatica-gerador", "key" => "Quadro de transferência automática gerador", "desc" =>"");
    
    $vetKey[51] = array("url" => "quadro-transferencia-automatica-gerador-preco", "key" => "Quadro de transferência automática gerador preço", "desc" =>"");
    
    $vetKey[52] = array("url" => "reforma-grupo-geradores", "key" => "Reforma de grupo geradores", "desc" =>"");
    
    $vetKey[53] = array("url" => "reforma-grupo-geradores-preco", "key" => "Reforma de grupo geradores preço", "desc" =>"");
    
    $vetKey[54] = array("url" => "tratamento-acustico-geradores", "key" => "Tratamento acústico para geradores", "desc" =>"");
    
  ?>
  