<?
$nomeSite			= 'Geradiesel';
$slogan				= 'Soluções em Energia';
// $url				= 'http://localhost/geradiesel.com.br/';
// $url				= 'http://www.mpitemporario.com.br/projetos/geradiesel.com.br/';
$url				= 'https://www.geradiesel.com.br/';
$ddd				= '11';
$fone				= '2951-7413';
// $fone2				= '7721-1940';
// $fone2				= '2983-1002';
// $fone3				= '7721-1939';
$emailContato		= 'atendimento@geradiesel.com.br';
$rua				= 'R. Cabo João Monteiro da Rocha, 439';
$bairro				= 'Jardim Japão';
$cidade				= 'São Paulo';
$UF					= 'SP';
$cep				= 'CEP: 02142-020';
$latitude			= '-23.508098';
$longitude			= '-46.5795377';
$idCliente			= '101583';
$idAnalytics		= 'UA-121443457-17';
$senhaEmailEnvia	= '102030'; // colocar senha do e-mail mkt@dominiodocliente.com.br
$explode			= explode("/", $_SERVER['PHP_SELF']);
$urlPagina 			= end($explode);
$urlPagina	 		= str_replace('.php','',$urlPagina);
$urlPagina 			== "index"? $urlPagina= "" : "";
$urlAnalytics = str_replace("http://www.", '', $url);
$urlAnalytics = str_replace("/", '', $urlAnalytics);
//reCaptcha do Google
$siteKey = '6LdONF8UAAAAAGbu2cdNM4IfeoIKnDg0C0xVUvpV';
$secretKey = '6LdONF8UAAAAAOOmA-am2-cd5_SlUbNQmiPU1G1O';
//********************COM SIG APAGAR********************
//Gerar htaccess automático
// $urlhtaccess = 'doutorestemporario.com.br/projetos/site_base_full_beta'; //SEM BARRA NO FINAL EM SEM HTTP://WWW.
$urlhtaccess = 'geradiesel.com.br';
define('RAIZ', $url);
define('HTACCESS', $urlhtaccess);
include('inc/gerador-htaccess.php');
//********************FIM SIG APAGAR********************
//Breadcrumbs
$caminho 			= '
<div id="breadcrumb" itemscope itemtype="http://data-vocabulary.org/Breadcrumb" >
	<a rel="home" itemprop="url" href="'.$url.'" title="Home"><span itemprop="title"><i class="fa fa-home" aria-hidden="true"></i> Home</span></a> »
	<strong><span class="page" itemprop="title" >'.$h1.'</span></strong>
	
</div>
';
$caminho2	= '
<div id="breadcrumb" itemscope itemtype="http://data-vocabulary.org/Breadcrumb" >
	<a rel="home" href="'.$url.'" title="Home" itemprop="url"><span itemprop="title"><i class="fa fa-home" aria-hidden="true"></i> Home</span></a> »
	<div itemprop="child" itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
		<a href="'.$url.'informacoes" title="Informações" class="category" itemprop="url"><span itemprop="title"> Informações </span></a> »
		<div itemprop="child" itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
			<strong><span class="page" itemprop="title" >'.$h1.'</span></strong>
		</div>
	</div>
</div>
';
//Pasta de imagens, Galeria, url Facebook, etc.
$pasta 				= 'imagens/informacoes/';
//Redes sociais
$idFacebook			= 'Colocar o ID da página do Facebook'; //Link para achar o ID da página do Facebook http://graph.facebook.com/Nome da página do Facebook
$idGooglePlus		= 'http://plus.google.com.br'; // ID da página da empresa no Google Plus
// $paginaFacebook		= 'PAGINA DO FACEBOOK DO CLIENTE';
$author = ''; // Link do perfil da empresa no g+ ou deixar em branco
//Reescrita dos links dos telefones
$link_tel = str_replace('(', '', $ddd);
$link_tel = str_replace(')', '', $link_tel);
$link_tel = str_replace('11', '', $link_tel);
$link_tel .= '5511'.$fone;
$link_tel = str_replace('-', '', $link_tel);
$creditos			= 'Doutores da Web - Marketing Digital';
$siteCreditos		= 'www.doutoresdaweb.com.br';
$caminhoBread 			= '
<div class="title-breadcrumb">
	<div class="wrapper">
		<div id="breadcrumb" itemscope itemtype="http://data-vocabulary.org/Breadcrumb" >
			<a rel="home" itemprop="url" href="'.$url.'" title="Home"><span itemprop="title"><i class="fa fa-home" aria-hidden="true"></i> Home</span></a> »
			<strong><span class="page" itemprop="title" >'.$h1.'</span></strong>
		</div>
		<h1>'.$h1.'</h1>
	</div>
</div>
';
$caminhoBread2	= '
<div class="title-breadcrumb">
	<div class="wrapper">
		<div id="breadcrumb" itemscope itemtype="http://data-vocabulary.org/Breadcrumb" >
			<a rel="home" href="'.$url.'" title="Home" itemprop="url"><span itemprop="title"><i class="fa fa-home" aria-hidden="true"></i> Home</span></a> »
			<div itemprop="child" itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
				<a href="'.$url.'informacoes" title="Informações" class="category" itemprop="url"><span itemprop="title"> Informações </span></a> »
				<div itemprop="child" itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
					<strong><span class="page" itemprop="title" >'.$h1.'</span></strong>
				</div>
			</div>
		</div>
		<h1>'.$h1.'</h1>
	</div>
</div>
';
?>