<div class="wrapper">
      <h2 class="h2-titulo">Produtos em Destaque</h2>
      <hr class="line">
      <ul class="thumbnails">
            <li>
                  <a rel="nofollow" href="<?=$url;?>empresa-manutencao-geradores" title="Empresa de manutenção de geradores"><img src="<?=$url;?>imagens/informacoes/empresa-manutencao-geradores-01.jpg" alt="Empresa de manutenção de geradores" title="Empresa de manutenção de geradores"/></a>
                  <h2><a href="<?=$url;?>empresa-manutencao-geradores" title="Empresa de manutenção de geradores">Empresa de manutenção de geradores</a></h2>
            </li>
            <li>
                  <a rel="nofollow" href="<?=$url;?>empresas-geradores-sp" title="Empresas de geradores em SP"><img src="<?=$url;?>imagens/informacoes/empresas-geradores-sp-01.jpg" alt="Empresas de geradores em SP" title="Empresas de geradores em SP"/></a>
                  <h2><a href="<?=$url;?>empresas-geradores-sp" title="Empresas de geradores em SP">Empresas de geradores em SP</a></h2>
            </li>
            <li>
                  <a rel="nofollow" href="<?=$url;?>instalacao-geradores-diesel" title="Instalação de geradores diesel"><img src="<?=$url;?>imagens/informacoes/instalacao-geradores-diesel-01.jpg" alt="Instalação de geradores diesel" title="Instalação de geradores diesel"/></a>
                  <h2><a href="<?=$url;?>instalacao-geradores-diesel" title="Instalação de geradores diesel">Instalação de geradores diesel</a></h2>
            </li>
            <li>
                  <a rel="nofollow" href="<?=$url;?>isolamento-acustico-geradores" title="Isolamento acústico de geradores"><img src="<?=$url;?>imagens/informacoes/isolamento-acustico-geradores-01.jpg" alt="Isolamento acústico de geradores" title="Isolamento acústico de geradores"/></a>
                  <h2><a href="<?=$url;?>isolamento-acustico-geradores" title="Isolamento acústico de geradores">Isolamento acústico de geradores</a></h2>
            </li>
      </ul>
</div>