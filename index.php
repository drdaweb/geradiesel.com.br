<?
$h1         = 'Gerardiesel';
$title      = 'Home';
$desc       = 'Gerardiesel: A empresa foi fundada em 1989 e durante esses anos tem mantido uma posição de destaque no mercado. Nossa principal especialização é manutenção';
$key        = 'uuuuuuuuuu, jjjjjjjjjjjj, lllllllllll';
$var        = 'Home';
include('inc/head.php');
?>
<style>
<?php include('hero/css/hero.css');
?>
</style>
<script>
<?php
include('hero/js/modernizr.js');
include('hero/js/main.js');
?>
</script>
<script>
<?php include('js/owl.carousel.js'); ?>
<?php include('js/slide-carousel.js'); ?>
</script>
<style>
<?php include('css/owl.carousel.css') ?>
<?php include('css/owl.theme.css') ?>
</style>
<!-- <link rel="stylesheet" href="<?=$url?>css/owl.carousel.css">
<link rel="stylesheet" href="<?=$url?>css/owl.theme.css">
<script src="<?=$url?>js/owl.carousel.js" ></script>
<script src="<?=$url?>js/slide-carousel.js"></script> -->
<script>
$(document).ready(function() {
$(".owl-demo").owlCarousel({
autoPlay: 3000,
items : 5, // Quantidade de itens que irá aparecer
itemsDesktopSmall : [979,4],
itemsTablet : [768, 3],
itemsMobile : [600, 1]
});
});
</script>
</head>
<body>
<? include('inc/topo.php'); ?>
<section class="cd-hero hide-mobile">
	<ul class="cd-hero-slider autoplay">
		<li class="selected">
			<a href="<?=$url?>manutencao-grupo-gerador-sp" title="Manutenção de geradores"><div class="cd-full-width">
					
				</div></a>
		</li>
		<li>
			<a href="<?=$url?>empresa-geradores" title="Empresa de geradores"><div class="cd-full-width">
					
				</div></a>
		</li>
		<li>
			<a href="<?=$url?>manutencao-grupo-gerador-sp" title="Manutenção geradores a diesel"><div class="cd-full-width">
					
				</div></a>
		</li>
	</ul>
	<!--   <div class="cd-slider-nav">
			<nav>
					<span class="cd-marker item-1"></span>
					<ul>
							<li class="selected"><a href="#0"><i class="fa fa-circle" aria-hidden="true"></i></a></li>
							<li><a href="#0"><i class="fa fa-circle" aria-hidden="true"></i></a></li>
							<li><a href="#0"><i class="fa fa-circle" aria-hidden="true"></i></a></li>
					</ul>
			</nav>
	</div> -->
	<div class="bg-hero">
		<div class="wrapper">
			<div class="bg-roxo">
				<h2>QUAL A SUA NECESSIDADE</h2>
				<p>Contratos de Manutenção Preventiva, Assistência Técnica, Instalação e Entrega Técnica de Grupos Geradores. Isolamento Acústico de Grupos Geradores.</p>
			</div>
		</div>
	</div>
</section>
<main>
	<div class="content">
		<section>
			<div class="wrapper">
				<h1 class="txtcenter h1-home"><?=$nomeSite." - ".$slogan?></h1>
				<h2 class="h2-titulo">Serviços</h2>
				<hr class="line">
				<div class=" grid">
					<a href="<?=$url?>servicos" title="Contratos de Manutenção Preventiva">
						<div class="col-4 thumb-text">
							<img class="img-icone picture-center" src="<?=$url;?>imagens/manutencao-preventiva.png" alt="Contratos de Manutenção Preventiva" title="Contratos de Manutenção Preventiva"/>
							<h2 class="txtcenter">Contratos de Manutenção Preventiva</h2>
							<p>Mediante Contrato de Manutenção, nossos clientes são visitados periodicamente, para que seja verificado o funcionamento do grupo gerador e o estado geral de suas peças...</p>
						</div>
					</a>
					<a href="<?=$url?>servicos" title="Assistência Técnica">
						<div class="col-4 thumb-text">
							<img class="img-icone picture-center" src="<?=$url;?>imagens/visita-tecnica.png" alt="Assistência Técnica" title="Assistência Técnica"/>
							<h2 class="txtcenter">Assistência Técnica</h2>
							<p>Nossos técnicos estão habilitados para fazer levantamentos, verificar defeitos, efetuar revisões periódicas, consertos e orientar quanto ao funcionamento do equipamento...</p>
						</div>
					</a>
					<a href="<?=$url?>servicos" title="Instalação e Entrega Técnica">
						<div class="col-4 thumb-text">
							<img class="img-icone picture-center" src="<?=$url;?>imagens/entrega-tecnica.png" alt="Instalação e Entrega Técnica" title="Instalação e Entrega Técnica"/>
							<h2 class="txtcenter">Instalação e Entrega Técnica</h2>
							<p>Combinando nossa experiência de mais de 20 anos no mercado, nosso projeto de instalação e entrega técnica...</p>
						</div>
					</a>
				</div>
				<hr>
				<div class="clear"></div>
				<div class="col-8">
					<h2 class="h2-titulo">Empresa</h2>
					<hr class="line">
					<img class="picture-center" src="<?=$url;?>imagens/empresa.jpg" alt="Conheça a Gerardisel" title="Conheça a Gerardisel"/>
					<p>A empresa foi fundada em 1989 e durante esses anos tem mantido uma posição de destaque no mercado. Nossa principal especialização é manutenção preventiva e corretiva em grupos geradores. Nossa empresa dispõe de profissionais altamente qualificados que estão constantemente melhorando seus métodos e conhecimentos.</p>
					<p>Atendemos às necessidades de nossos clientes com máxima prioridade. Isto é o que nos coloca como ponto de referência no mercado.</p>
					<p class="txtleft"><a href="<?=$url?>empresa" title="Sobre a Gerardiesel" class="btn"> Saiba mais sobre a Gerardiesel<i class="fa fa-external-link" aria-hidden="true"></i></a></p>
				</div>
				<div class="col-4">
					<h2 class="h2-titulo">CURTA NOSSA FANPAGE</h2>
					<iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fgeradieselgeradores&tabs=timeline&width=340&height=500&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId=348554161887770" style="border:none;overflow:hidden;width: 100%;height: 350px;" ></iframe>
				</div>
				<hr>
				<div class="clear"></div>
			</div>
			<div class="clear"></div>
			<? include('inc/produtos-destaques-clientes.php'); ?>
			<div class="clear"></div>
		</section>
	</div>
</main>
<? include('inc/footer.php'); ?>
<!-- <link rel="stylesheet" href="<?=$url?>hero/css/hero.css">
<script src="<?=$url?>hero/js/modernizr.js"></script>
<script src="<?=$url?>hero/js/main.js"></script> -->
</body>
</html>