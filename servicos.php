<?
$h1         = 'Serviços';
$title      = 'Serviços';
$desc       = 'Serviços: A empresa foi fundada em 1989 e durante esses anos tem mantido uma posição de destaque no mercado. Nossa principal especialização é manutenção';
$key        = 'uuuuuuuuuu, jjjjjjjjjjjj, lllllllllll';
$var        = 'Serviços';
include('inc/head.php');
?>
<script src="<?=$url?>js/owl.carousel.js" ></script>
<link rel="stylesheet" href="<?=$url?>css/owl.carousel.css">
<link rel="stylesheet" href="<?=$url?>css/owl.theme.css">
<script>
$(document).ready(function() {
$(".owl-demo").owlCarousel({
autoPlay: 3000,
items : 5, // Quantidade de itens que irá aparecer
itemsDesktopSmall : [979,4],
itemsTablet : [768, 3],
itemsMobile : [600, 1]
});
});
</script>
</head>
<body>
<? include('inc/topo.php');?>
<main>
    <div class="content">
        <section>
            <div class="wrapper">
                <?=$caminho?>
                <h1><?=$h1?></h1>
                <hr class="line">
            </div>
            <article class="full">
                
                <div class="wrapper">
                    <div class="grid  ">
                        <div class="col-12  flex thumb-text">
                            <div class="col-6">
                                <img class="img-icone picture-center" src="<?=$url;?>imagens/manutencao-preventiva.png" alt="Contratos de Manutenção Preventiva" title="Contratos de Manutenção Preventiva"/>
                            </div>
                            <div class="col-6">
                                <h2 >Contratos de Manutenção Preventiva</h2>
                                <p>Mediante Contrato de Manutenção, nossos clientes são visitados periodicamente, para que seja verificado o funcionamento do grupo gerador e o estado geral de suas peças...</p>
                            </div>
                        </div>
                        <div class="col-12 flex thumb-text">
                            <div class="col-6">
                                <img class="img-icone picture-center" src="<?=$url;?>imagens/visita-tecnica.png" alt="Assistência Técnica" title="Assistência Técnica"/>
                            </div>
                            <div class="col-6">
                                <h2 >Assistência Técnica</h2>
                                <p>Nossos técnicos estão habilitados para fazer levantamentos, verificar defeitos, efetuar revisões periódicas, consertos e orientar quanto ao funcionamento do equipamento...</p>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <div class="col-12 flex thumb-text">
                            <div class="col-6">
                                <img class="img-icone picture-center" src="<?=$url;?>imagens/entrega-tecnica.png" alt="Instalação e Entrega Técnica" title="Instalação e Entrega Técnica"/>
                            </div>
                            <div class="col-6">
                                <h2 >Instalação e Entrega Técnica</h2>
                                <p>Combinando nossa experiência de mais de 20 anos no mercado, nosso projeto de instalação e entrega técnica...</p>
                            </div>
                        </div>
                        <div class="col-12 flex thumb-text">
                            <div class="col-6">
                                <img class="img-icone picture-center" src="<?=$url;?>imagens/grupos-geradores.png" alt="Instalação e Entrega Técnica" title="Instalação e Entrega Técnica"/>
                            </div>
                            <div class="col-6">
                                <h2> Isolamento acústico da sala do grupo gerador </h2>
                                <p>Aplicável na sala do grupo gerador minimizando o som/barulho interno e melhorando o conforto. Executamos este serviço dentro dos padrões ABNT.</p>
                                <h2>Isolamento acústico do grupo gerador</h2>
                                <p>Se o seu gerador é aberto, projetamos e executamos cabine tipo container e aplicamos o mesmo sistema utilizado no isolamento acústico da sala. Podendo deixar seu grupo gerador exposto ao tempo, sem necessidade de sala.</p>
                            </div>
                        </div>
                    </div>
                    
                    
                </div>
                <div class="clear"></div>
                <? include('inc/produtos-destaques-clientes.php'); ?>
                <div class="clear"></div>
                
            </article>
        </section>
    </div>
</main>
<? include('inc/footer.php');?>
</body>
</html>