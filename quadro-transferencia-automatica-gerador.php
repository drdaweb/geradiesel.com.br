
      <?php
      include('inc/vetKey.php');
      $h1             = "Quadro de transferência automática gerador";
      $title          = $h1;
      $desc           = "O quadro de transferência automática gerador pode ser acionado. No momento em que esta energia é restabelecida, também é possível contar com o QTA.";
      $key            = "quadro,transferencia,automatica,gerador";
      $legendaImagem  = "Foto ilustrativa de Quadro de transferência automática gerador";
      $pagInterna     = "Informações";
      $urlPagInterna  = "informacoes";
      include('inc/head.php');
      include('inc/fancy.php');
      ?>
      <script defer src="<?=$url?>js/organictabs.jquery.js" ></script>
    <?php include("inc/type-search.php")?>
</head>
    <body>
      
      <? include('inc/topo.php');?>
      <div class="wrapper">
        <main>
          <div class="content">
            <section>
              <?=$caminho2?>
              <h1><?=$h1?></h1>
              <article>
                <? $quantia = 3; include('inc/gallery.php');?>
                <p class="alerta">Clique nas imagens para ampliar</p>
                <h2>O QUADRO DE TRANSFERÊNCIA AUTOMÁTICA GERADOR É SEGURO E EFICIENTE</h2>

<p>Segurança e eficiência são apenas duas das principais características primeiras a marcar presença em um <strong>quadro de transferência automática gerador</strong>. Tecnicamente, este dispositivo (que também é conhecido como QTA) se trata de um painel elétrico que permite o acionamento de qualquer modelo de grupo gerador de maneira automatizada. Internamente ao dispositivo, diversas correntes, abertos ou em rampa podem suprir necessidades específicas em espaços como eventos, grandes indústrias ou operações de menor porte. Por se tratar de um objeto automático, o conceito de tecnologia também é fortemente destacado nestas estruturas.</p>

<p>Através de uma prática pré-programada, é possível ativar um grupo gerador (principalmente se o mesmo for movido à diesel). Com o gesto, também é provável que se supra a falta de energia promovida pela concessionária ou pela simples necessidade de operação que eventualmente marca presença em qualquer evento ou espaço comercial/industrial. Como o manuseio do equipamento é automatizado, a tendência é a de que o <strong>quadro de transferência automática gerador</strong> seja de aplicação cada vez mais prática independentemente do local em que a peça se submeta.</p>

<h3>O QUADRO DE TRANSFERÊNCIA AUTOMÁTICA GERADOR É DE UTILIZAÇÃO ABSOLUTAMENTE DINÂMICA</h3>

<p>Dinamismo é uma das principais características propostas pelo <strong>quadro de transferência automática gerador</strong> e o gesto se explica da seguinte maneira:</p>

<p>Assim que a falta de energia assola um espaço, o <strong>quadro de transferência automática gerador</strong> pode ser acionado. No momento em que esta energia é restabelecida, também é possível contar com o QTA. A partir da programação do dispositivo, é possível, portanto, desligar o gerador de energia movido a diesel ou a qualquer outro combustível. Nestes casos, no entanto, é recomendável resguardar um tempo extra para a consolidação da atividade. Esse tempo tenderá a oferecer uma segurança mais otimizada ao processo. </p>

<h3>QUADRO DE TRANSFERÊNCIA AUTOMÁTICA GERADOR É COM A GERADIESEL!</h3>

<p>A Geradiesel integra diversas plataformas tecnológicas em sua rotina produtiva/de prestação de serviços e, desta maneira, o <strong>quadro de transferência automática gerador</strong> se coloca como um dos dispositivos de maior índice de comercialização e manutenção propostos pela empresa. São mais de 25 anos de expertise na área.</p>



                <? include('inc/saiba-mais.php');?>
                <? include('inc/social-media.php');?>
              </article>
              <? include('inc/coluna-lateral.php');?>
              <br class="clear" />
              <? include('inc/paginas-relacionadas.php');?>
              <? include('inc/regioes.php');?>
              <br class="clear">
              <? include('inc/copyright.php');?>
            </section>
          </div>
        </main>
      </div><!-- .wrapper -->
      <? include('inc/footer.php');?>
    </body>
    </html>